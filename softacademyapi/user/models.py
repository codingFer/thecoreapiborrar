from django.db import models

from softacademyapi.user.mixins import TimeStampedModel

class User(TimeStampedModel):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    cellphone = models.CharField(max_length=15,blank=True, null=True)
    gender = models.BooleanField(blank=True, null=True)
    born = models.DateField(blank=True, null=True)
    
    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
