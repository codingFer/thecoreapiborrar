from django.shortcuts import render

from softacademyapi.user.mixins import DefaultViewSetMixin, ModelViewSetMixin
from softacademyapi.user.models import User
from softacademyapi.user.serializers import UserSerializer

class UserViewSet(DefaultViewSetMixin, ModelViewSetMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = ('email',)