from rest_framework import serializers
from django.contrib.auth.models import User

from softacademyapi.user.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')
